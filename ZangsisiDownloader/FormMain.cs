﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.IO;
using System.Net;
using System.Threading;
using System.Drawing.Imaging;

namespace ZangsisiDownloader
{
    public partial class FormZangsisi : Form
    {
        public FormZangsisi()
        {
            InitializeComponent();
        }

        private bool urlChecker(string url)
        {
            return Regex.IsMatch(url.ToLower(), "http://(e.)?zangsisi.tumblr.com/post/.*");
        }

        private void downloadImages(string[] url, int count, string path)
        {
            string title = Path.GetFileName(path);
            statusProgress.Maximum = count;
            statusLblProgress.Text = 0 + "/" + count;
            statusProgress.Value = 0;
            statusLabel.Text = "Downloading " + title;
            using (WebClient client = new WebClient())
            {
                for (int i = 1; i <= count; i++)
                {
                    string downloadPath = Path.Combine(path, i.ToString("000"));
                    byte[] data = client.DownloadData(url[i - 1]);
                    using (MemoryStream mem = new MemoryStream(data))
                    {
                        Image tmpImage = Image.FromStream(mem);
                        if (tmpImage.RawFormat.Equals(System.Drawing.Imaging.ImageFormat.Jpeg))
                            tmpImage.Save(downloadPath + ".jpg");
                        else if (tmpImage.RawFormat.Equals(System.Drawing.Imaging.ImageFormat.Gif))
                            tmpImage.Save(downloadPath + ".gif");
                        else
                        {
                            var encoder = ImageCodecInfo.GetImageEncoders().First(c => c.FormatID == ImageFormat.Jpeg.Guid);
                            var encParams = new EncoderParameters() { Param = new[] { new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, 90L) } };
                            tmpImage.Save(downloadPath + ".jpg", encoder, encParams);
                        }
                    }
                    statusLblProgress.Text = i + "/" + count;
                    statusProgress.Value = i;
                }
            }
            statusLabel.Text = "Download complete " + title;
        }

        private void resetStatus(string text)
        {
            statusProgress.Maximum = 1;
            statusProgress.Value = 0;
            statusLblProgress.Text = "";
            statusLabel.Text = text;
        }

        private void lblUrl_Click(object sender, EventArgs e)
        {
            txtUrl.Focus();
        }

        private void txtUrl_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btnDown_Click(sender, e);
        }

        private void label1_Click(object sender, EventArgs e)
        {
            txtPath.Focus();
        }

        private void FormZangsisi_Load(object sender, EventArgs e)
        {
            string url = Clipboard.GetText();
            if (urlChecker(url)) {
                txtUrl.Text = url;
            }
            txtPath.Text = Path.GetDirectoryName(Application.ExecutablePath);
//            txtPath.Text = Environment.GetEnvironmentVariable("USERPROFILE") + @"\" + "Downloads";
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            fbd.SelectedPath = txtPath.Text;
            DialogResult result = fbd.ShowDialog();
            if (result == DialogResult.OK)
            {
                txtPath.Text = fbd.SelectedPath;
            }
        }

        private void btnDown_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrWhiteSpace(txtUrl.Text))
                return;
            if (!urlChecker(txtUrl.Text))
            {
                resetStatus("Invalid Url!");
                return;
            }
            resetStatus("Parsing Contents");
            using (WebClient client = new WebClient())
            {
                client.Encoding = Encoding.UTF8;

                string content = client.DownloadString(txtUrl.Text);
                Match title = Regex.Match(content, "var title = \"(.*?)\";", RegexOptions.IgnoreCase);
                if (!title.Success)
                {
                    resetStatus("Downlaod failed");
                    return;
                }

                Match result = Regex.Match(content, "<div class=\"postbody\"[^>]*?>(.*?)</div>.*?<div class=\"bottom\"", RegexOptions.IgnoreCase | RegexOptions.Singleline);
                if (!result.Success)
                {
                    resetStatus("Downlaod failed");
                    return;
                }
                content = result.Groups[0].ToString();
                MatchCollection images = Regex.Matches(content, "<img[^>]*?src=[\"\'](.*?)[\"\']");
                if (images.Count == 0)
                {
                    resetStatus("Downlaod failed");
                    return;
                }
                string[] imageUrls = new string[images.Count];
                for (int i = 0; i < images.Count; i++) {
                    imageUrls[i] = images[i].Groups[1].Value;
                }
                string path = Path.Combine(txtPath.Text, title.Groups[1].Value);
                try {
                    DirectoryInfo createResult = Directory.CreateDirectory(path);
                }
                catch {
                    resetStatus("Downlaod failed");
                    return;
                }
                Thread downloadThread = new Thread(() => downloadImages(imageUrls, images.Count, path));
                downloadThread.Start();
            }
        }

        private void FormZangsisi_ClipboardChk(object sender, EventArgs e)
        {
            string url = Clipboard.GetText();
            if (urlChecker(url))
            {
                txtUrl.Text = url;
            }
        }

        private void txtPath_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btnSelect_Click(sender, e);
        }
    }
}
